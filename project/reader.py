'''
| Project:          Moduleit-M5Stack
| File:             reader.py
| Author:           Abdulrahman Othman
| Created:          01.08.2023
| Last Modified:    03.02.2024
'''
from machine import UART, Pin
from time import sleep_ms

'''
' M5Stack Commands
'''
M5C = {
    "hardware_version": [0xBB, 0x00, 0x03, 0x00, 0x01, 0x00, 0x04, 0x7E],
    "start_multi_plling": [0xBB, 0x00, 0x27, 0x00, 0x03, 0x22, 0x27, 0x10, 0x83, 0x7E],
    "start_single_polling": [0xBB, 0x00, 0x22, 0x00, 0x00, 0x22, 0x7E],
    "stop_multid_polling": [0xBB, 0x00, 0x28, 0x00, 0x00, 0x28, 0x7E]
}
'''
' Reader Class to run M5Stack
'''


class Reader:
    '''
    ' In Constructor only object instance of UART Protocol
    '''

    def __init__(self, baudrate: int, tx_pin: int, rx_pin: int):
        self.__uart = UART(0, baudrate=baudrate, tx=Pin(tx_pin), rx=Pin(rx_pin))
        self.init_reader()

    '''
    ' Send command to read tags
    '''

    def _send_command(self, command: list):
        self.__uart.write(bytearray(command))
        sleep_ms(50)  # Allow time for the command to be processed

    '''
    ' Init function to check reader functionality
    '''

    def init_reader(self):
        self._send_command(M5C['hardware_version'])
        response = self.__uart.read()
        if response is not None and b"26dBm" in response:
            return "Reader Init Success"
        else:
            return "Init Error => {}".format(response)

    '''
    ' catch epc's once available reader functionality
    '''

    def read_epc(self):
        if self.__uart.any():
            resp = self.__uart.read()
            # if len(resp) == 24:
            if len(resp) > 10:
                return resp
        else:
            self._send_command(M5C['start_single_polling'])
            return False
