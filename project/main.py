'''
@author Abdulrahman Othman
@email  abdulrahman.othman@web.de
@date   10.01.2023
@brief  Blind UHF Project
@device Rasp Pi Pico W MicroPython
@UHF    M5Stack
'''
from machine import Pin, PWM, RTC
from bluetooth import BLE
from ble_simple_peripheral import BLESimplePeripheral
from time import sleep, sleep_ms
from reader import Reader

# Objects
power_button = Pin(15, Pin.IN, Pin.PULL_DOWN)
buzzer_pin = Pin(14, Pin.OUT)
m5_power = Pin(13)
m5_power.init(Pin.OUT)
m5_power.value(0)

buzzer = PWM(buzzer_pin)
# Create a Bluetooth Low Energy (BLE) object
ble = None
# Create an instance of the BLESimplePeripheral class with the BLE object
sp = None
# M5Stack Module object
reader = None
xPower = False
rtc = RTC()


def response(message: str):
    return "Reponse[" + message + "]"


def play_tone(frequency, duration, duty):
    buzzer.freq(frequency)
    buzzer.duty_u16(duty)
    sleep_ms(duration)
    buzzer.duty_u16(0)
    sleep_ms(duration)


last_action = rtc.datetime()
while True:
    if power_button.value():
        sleep(0.5)
        if xPower:
            xPower = False
            # sp.send(response("Device + Bluetooth Disconnected"))
            sp.desonnect()
            ble = None
            sp = None
            reader = None
            play_tone(300, 250, 4000)
            sleep(0.1)
            play_tone(250, 200, 4000)
            play_tone(200, 150, 4000)
            m5_power.value(0)
            # machine.freq(20000000)
        else:
            # machine.freq()
            xPower = True
            ble = BLE()
            sp = BLESimplePeripheral(ble)
            # if sp.is_connected():
            #     sp.send(str(response("Device + Bluetooth Connected")))
            reader = Reader(115200, 12, 1)
            reader.init_reader()
            play_tone(500, 100, 5000)
            play_tone(400, 200, 4000)
            play_tone(300, 300, 4000)
            m5_power.value(1)
    if xPower:
        r = reader.read_epc()
        if r:
            print(r)
            play_tone(500, 110, 4000)
            if sp.is_connected():
                #sp.send("00")
                sp.write(str(r))
