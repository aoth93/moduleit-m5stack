# ModuleIT-M5Stack

## Projektbeschreibung

Blindenstock, dieser soll Leitsysteme mittels Radiowellen in Bodenstreifen erfassen und über ein Lesegerät im Blindenstock lesen.

## Physische Materialien

- Raspberry Pi Pico W mit Bluetooth und WIFI
- Auflademodul mit USB Type-C
- DCDC Umwandler
- Akku 1300mAh
- Buzzer Ton


## Entwicklungsumgebung

- Programmiersprache Python v3.11
- Framework MicroPython
- Thonny IDE für Micropython

## Nutzungseinleitung
1. Download and install 3.11
2. Thonny IDE ausführen
3. Microcontroller mit dem PC über MicroUSB verbinden
4. Microcontroller in Thonny auswählen
5. Die folgenden Dateien auf Microcontroller übertragen
6. main.py im Microcontroller starten [ble_advertising, ble_simple_peripheral, main, reader]
7. Enjoy it

## Programminhalt
- ble_advertising: Dies initialisiert die Peripheriegeräte für die Verbindungsherstellung über BLE (Bluetooth Low Energy).
- ble_simple_peripheral: Hier werden die obersten Komponenten des Programms ausgeführt. 
- reader: Dieses Programm ist für den M5Stack UHF-RFID-Reader zuständig und enthält Methoden zum Lesen von Daten sowie zur Durchführung von Pausen. 
- main: Dies ist die Hauptdatei, in der das gesamte Programm verwaltet wird. 
  - Hier werden der Programmstart, die Tonsteuerung und die Datensendung über Bluetooth verwaltet.

<img src="https://www.raspberrypi.com/documentation/microcontrollers/images/pico-pinout.svg">

## Physische Einleitung
GND Pins könnten an alle GNDs am Microcontroller verbunden werden 
- Run/Standby Knopf
  - [+] GP15 -> 20
  - [-] GND
- Buzzer Ton
  - [+] GP14 -> 19
  - [-] GND
- M5Stack Modul
  - [+] GP13 -> 17
  - [-] GND
- 